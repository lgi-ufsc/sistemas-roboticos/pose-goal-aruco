#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>

#include <cmath>


// variável global com a posição
geometry_msgs::Pose ar_pose;


// qual a identificação das marcas a serem lidas
#define marker_1  90
#define marker_2  25
#define marker_3  20


// Função mod_vet
// Recebe: um vetor e seu tamanho
// Retorna: o módulo do vetor 
float mod_vet(float vet[3]){
	float somaq = 0.0;

	// soma dos quadrados dos termos do vetor
  for(int i= 0 ; i<3 ; i++ ){
    somaq += pow(vet[i], 2);
  }

	// raiz  da soma dos quadrados dos termos do vetor
	return pow(somaq, 0.5);
}


void chatterCallback(const visualization_msgs::Marker& msg){ 	

  // id  =>  identificação da marca
	int id = msg.id;

	// Pos1  =>  vetor posição na plataforma do marker_1
	// Pos2  =>  vetor posição na plataforma do marker_2
	// Pos3  =>  vetor posição na plataforma do marker_3
	static float Pos1[3], Pos2[3], Pos3[3];

	// Ori1  =>  vetor orientação na plataforma do marker_1
	// Ori2  =>  vetor orientação na plataforma do marker_2
	// Ori3  =>  vetor orientação na plataforma do marker_3
	static float Ori1[3], Ori2[3], Ori3[3];
  
	// vet_12      =>  vetor da posição do marker_2 ao marker_1
	// vet_32      =>  vetor da posição do marker_2 ao marker_3 
	// vet_proj    =>  vetor da projeção de vet_32 em vet_12
	// vet_gram    =>  vetor para ortogonalizaçãode Gram-Schgimit
	static float vet_12[3], vet_32[3], vet_proj[3], vet_gram[3];

	// mod_12    =>  módulo do vet_12
	// mod_gram  =>  módulo do vet_gram
	// mod_proj  =>  módulo do vet_proj
	float mod_12, mod_gram, mod_proj;
	
	// prod_es   =>  produto escalar de vet_32 . vet_12
	float prod_es = 0.0;

	// norm_proj  =>  norma do vet_proj
	// norm_gram  =>  norma do vet_gram
	// prod_vet   =>  produto vetorial de norm_gram X norm_proj
	static float norm_proj[3], norm_gram[3], prod_vet[3];

	// OriCal  => vetor orientação calculada 
	static float OriCal[3];

  int i;
  
  //+-------------------------------------------------------------------------------+
  // Identificando as marcas
  // ^^^^^^^^^^^^^^^^^^^^^^^
	if( id == marker_1 ){

		Pos1[0] = msg.pose.position.x;
		Pos1[1] = msg.pose.position.y;
		Pos1[2] = msg.pose.position.z;

 		//std::cout << "ID:" << id << std::endl;
    //std::cout << "Marker " << marker_1 << " identificado" << std::endl;
    
	}
	if( id == marker_2 ){

    Pos2[0] = msg.pose.position.x;
		Pos2[1] = msg.pose.position.y;
		Pos2[2] = msg.pose.position.z;

		//std::cout << "ID:" << id << std::endl;
		//std::cout << "Marker " << marker_2 << " identificado" << std::endl;
    
	}
	if( id == marker_3 ){

		Pos3[0] = msg.pose.position.x;
		Pos3[1] = msg.pose.position.y;
		Pos3[2] = msg.pose.position.z;

		//std::cout << "ID:" << id << std::endl;
		//std::cout << "Marker " << marker_3 << " identificado" << std::endl;
    
	}
  //+-------------------------------------------------------------------------------+
  // Printa os vetores posições
  // ^^^^^^^^^^^^^^^^^^^^^^^^^^
	//std::cout << "Position marker " << marker_1 << " : " << "[ " << Pos1[0] << " , " << Pos1[1] << " , " << Pos1[2] << " ]" << std::endl;
	//std::cout << "Position marker " << marker_2 << " : " << "[ " << Pos2[0] << " , " << Pos2[1] << " , " << Pos2[2] << " ]" << std::endl;
	//std::cout << "Position marker " << marker_3 << " : " << "[ " << Pos3[0] << " , " << Pos3[1] << " , " << Pos3[2] << " ]" << std::endl;
  // Printa os vetores orientações
	//std::cout << "Orientation marker " << marker_1 << " : " << "[ " << Ori1[0] << " , " << Ori1[1] << " , " << Ori1[2] << " ]" << std::endl;
	//std::cout << "Orientation marker " << marker_2 << " : " << "[ " << Ori2[0] << " , " << Ori2[1] << " , " << Ori2[2] << " ]" << std::endl;
	//std::cout << "Orientation marker " << marker_3 << " : " << "[ " << Ori3[0] << " , " << Ori3[1] << " , " << Ori3[2] << " ]" << std::endl;
  //+-------------------------------------------------------------------------------+
  // Cálculo da orientação
  // ^^^^^^^^^^^^^^^^^^^^^
	// Vetores do marker 2 ao 1 e do marker 2 ao 3
	for( i=0 ; i<3 ; i++ ){
		vet_12[i] = Pos1[i] - Pos2[i];
		vet_32[i] = Pos3[i] - Pos2[i];
	}
	// Módulo do vetor vet_12
	mod_12 = mod_vet(vet_12);
	// Produto escalar -> vet_32  . vet_12
	for( i=0 ; i<3 ; i++ ){
		prod_es += vet_32[i]*vet_12[i];
	}
	// Projeção de vet_32 em vet_12
	for( i=0 ; i<3 ; i++ ){
		vet_proj[i] = ( prod_es / pow(mod_12, 2) ) * vet_12[i];
	}
	// Módulo do vetor vet_proj
	mod_proj = mod_vet(vet_proj);
	// Ortogonalização de Gram-Schimidf
	for( i =0 ; i<3 ; i++){
		vet_gram[i] =  vet_32[i] - vet_proj[i];
	}
	// Módulo do vetor vet_gram
	mod_gram = mod_vet(vet_gram);
	// Vetores normalizadoss
	for( i=0 ; i<3 ; i++ ){
		norm_proj[i] = vet_proj[i]/mod_proj;
		norm_gram[1] = vet_gram[i]/mod_gram;
	}
	// Produto vetorial ->  norm_gram X norm_proj
	prod_vet[0] = ( norm_gram[1]*norm_proj[2] ) - ( norm_gram[2]*norm_proj[1] );
	prod_vet[1] = ( norm_gram[2]*norm_proj[0] ) - ( norm_gram[0]*norm_proj[2] );
	prod_vet[2] = ( norm_gram[0]*norm_proj[1] ) - ( norm_gram[1]*norm_proj[0] );
	// Extrair a orientação:
	for( i=0 ; i<3 ; i++ ){
		OriCal[i]  = norm_proj[i] + norm_gram[i] + prod_vet[i]; 
	}	
	//+-------------------------------------------------------------------------------+
	// Armazenando a posição aruco
  // ^^^^^^^^^^^^^^^^^^^^^^^^^^^

  ar_pose.orientation.x = OriCal[1];
  ar_pose.orientation.y = OriCal[2];
  ar_pose.orientation.z = OriCal[3];

	ar_pose.position.x = Pos2[0];
	ar_pose.position.y = Pos2[1];
	ar_pose.position.z = Pos2[2];

}


int main(int argc, char** argv){

  ROS_WARN("Iniciando Node");
  // iniciacao padrao para nodes
  ros::init(argc, argv, "PoseGoal");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();
  //* This sleep is ONLY to allow Rviz to come up */
  //sleep(2.0);
  //+-------------------------------------------------------------------------------+
  while (ros::ok()){
    ros::Duration(0.001).sleep(); // 0.001s = 1 ms
    // Pegando a posição aruco
    // ^^^^^^^^^^^^^^^^^^^^^^^
    ROS_WARN("Pegando a posicao da marca aruco");
    ros::Subscriber sub = node_handle.subscribe("/Estimated_marker", 1, chatterCallback);
    //+-------------------------------------------------------------------------------+
    // Setup
    // ^^^^^
    ROS_WARN("Iniciando PLANNING_GROUP");
    //
    // MoveIt operates on sets of joints called "planning groups" 
    // and stores them in an object called the `JointModelGroup`.
    // Throughout MoveIt the terms "planning group" and "joint model group" are used interchangably.
    static const std::string PLANNING_GROUP = "panda_arm";

    // The :move_group_interface:`MoveGroupInterface` class can be easily setup
    // using just the name of the planning group you would like to control and plan for.
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);

    // We will use the :planning_scene_interface:`PlanningSceneInterface` class
    // to add and remove collision objects in our "virtual world" scene
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

    // Raw pointers are frequently used to refer to the planning group for improved performance.
    const robot_state::JointModelGroup* joint_model_group =
    move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    //+-------------------------------------------------------------------------------+
    // Visualization
    // ^^^^^^^^^^^^^
    //
    ROS_WARN("Iniciando Visual_Tools");
    //
    // The package MoveItVisualTools provides many capabilties for visualizing objects, robots, and trajectories
    // in RViz as well as debugging tools such as step-by-step introspection of a script
    namespace rvt = rviz_visual_tools;
    moveit_visual_tools::MoveItVisualTools visual_tools("panda_link0");
    visual_tools.deleteAllMarkers();

    // Remote control is an introspection tool that allows users to 
    // step through a high level script via buttons and keyboard shortcuts in RViz
    visual_tools.loadRemoteControl();

    // Scales
    // XXXXSMALL = 1, XXXSMALL = 2, XXSMALL = 3, XSMALL = 4,
    // SMALL = 5, MEDIUM = 6, LARGE = 7, XLARGE = 8,
    // XXLARGE = 9, XXXLARGE = 10, XXXXLARGE = 11

    // Colors 
    // BLACK 	 BROWN 	 BLUE 	 CYAN 	 GREY 	 DARK_GREY 	 
    // GREEN 	 LIME_GREEN 	 MAGENTA 	 ORANGE 	 PURPLE 	 
    // RED 	 PINK 	 WHITE 	 YELLOW 	 
    // TRANSLUCENT 	 TRANSLUCENT_LIGHT 	 TRANSLUCENT_DARK 	 
    // RAND 	 CLEAR 	 DEFAULT 

    // RViz provides many types of markers, in this demo we will use text, cylinders, and spheres
    Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
    text_pose.translation().z() = 1.75;

    visual_tools.publishText(text_pose, "Iniciando Visual_Tools", rvt::WHITE, rvt::XLARGE);
    // (Optional) Create a publisher for visualizing plans in Rviz.
    ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/group/display_planned_path", 1, true);
    moveit_msgs::DisplayTrajectory display_trajectory;

    // Batch publishing is used to reduce the number of messages being sent to RViz for large visualizations
    visual_tools.trigger();
    //+-------------------------------------------------------------------------------+
    // Getting Basic Information
    // ^^^^^^^^^^^^^^^^^^^^^^^^^.
    move_group.getPlanningFrame().c_str();
    move_group.getEndEffectorLink().c_str();
    //+-------------------------------------------------------------------------------+
    // Planning to a Pose goal
    // ^^^^^^^^^^^^^^^^^^^^^^^  
    // We can plan a motion for this group to 
    // a desired pose for the end-effector.
    ROS_WARN("Iniciar Plano Pose Goal");
    visual_tools.deleteAllMarkers();
    visual_tools.publishText(text_pose, "Iniciar Plano Pose Goal", rvt::ORANGE, rvt::XXXLARGE);
    visual_tools.trigger();

    geometry_msgs::Pose target_pose1; 
    target_pose1.orientation.x =  ar_pose.orientation.x;
    target_pose1.orientation.y =  ar_pose.orientation.y;
    target_pose1.orientation.z =  ar_pose.orientation.z;
    target_pose1.position.x    =  ar_pose.position.x;
    target_pose1.position.y    =  ar_pose.position.y;
    target_pose1.position.z    =  ar_pose.position.z;
    move_group.setPoseTarget(target_pose1);

    // Se der problema no planejamento volte ao valor padrão
    // apagando a próxima linha de código  ou colocando 5.0s
    move_group.setPlanningTime(2.5);// 2.5s 

    // Now, we call the planner to compute the plan and visualize it.
    // Note that we are just planning, 
    // not asking group to actually move the robot.
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;

    bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    ROS_INFO_NAMED( "Visualizing plan pose goal %s", success ? "" : "FAILED");
    //+-------------------------------------------------------------------------------+
    // Visualizing plans
    // ^^^^^^^^^^^^^^^^^
    // We can also visualize the plan as a line with markers in RViz.
    visual_tools.publishAxisLabeled(target_pose1, "pose1");
    visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishTrajectoryLine(my_plan.trajectory_, joint_model_group);
    visual_tools.trigger();
    //+-------------------------------------------------------------------------------+
    //Executar no RViz
    // ^^^^^^^^^^^^^^^^^
    ROS_WARN("Executa Plano Pose Goal");
    // visual_tools.deleteAllMarkers();
    visual_tools.publishText(text_pose, "Executa Plano Pose Goal", rvt::ORANGE, rvt::XXXLARGE);
    visual_tools.trigger();
    move_group.execute(my_plan);
    //+-------------------------------------------------------------------------------+
    ROS_WARN("'Crl+C' para finalizar");
    // visual_tools.prompt("Presione 'next' no Rviz"); //com o next

  } 
}
