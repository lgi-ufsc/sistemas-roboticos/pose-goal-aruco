#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>

#include <cmath>


// qual a identificação das marcas a serem lidas
#define marker_1  90
#define marker_2  25
#define marker_3  20


// Função mod_vet
// Recebe: um vetor e seu tamanho
// Retorna: o módulo do vetor 
float mod_vet(float vet[3]){
	float somaq = 0.0;

	// soma dos quadrados dos termos do vetor
    for(int i= 0 ; i<3 ; i++ ){
        somaq += pow(vet[i], 2);
    }

	// raiz  da soma dos quadrados dos termos do vetor
	return pow(somaq, 0.5);
}


void chatterCallback(const visualization_msgs::Marker& msg){ 	

    // id  =>  identificação da marca
	int id = msg.id;

	// Pos1  =>  vetor posição na plataforma do marker_1
	// Pos2  =>  vetor posição na plataforma do marker_2
	// Pos3  =>  vetor posição na plataforma do marker_3
	static float Pos1[3], Pos2[3], Pos3[3];

	// Ori1  =>  vetor orientação na plataforma do marker_1
	// Ori2  =>  vetor orientação na plataforma do marker_2
	// Ori3  =>  vetor orientação na plataforma do marker_3
	static float Ori1[3], Ori2[3], Ori3[3];
  
	// vet_12      =>  vetor da posição do marker_2 ao marker_1
	// vet_32      =>  vetor da posição do marker_2 ao marker_3 
	// vet_proj    =>  vetor da projeção de vet_32 em vet_12
	// vet_gram    =>  vetor para ortogonalizaçãode Gram-Schgimit
	static float vet_12[3], vet_32[3], vet_proj[3], vet_gram[3];

	// mod_12    =>  módulo do vet_12
	// mod_gram  =>  módulo do vet_gram
	// mod_proj  =>  módulo do vet_proj
	float mod_12, mod_gram, mod_proj;
	
	// prod_es   =>  produto escalar de vet_32 . vet_12
	float prod_es = 0.0;

	// norm_proj  =>  norma do vet_proj
	// norm_gram  =>  norma do vet_gram
	// prod_vet   =>  produto vetorial de norm_gram X norm_proj
	static float norm_proj[3], norm_gram[3], prod_vet[3];

	// OriCal  => vetor orientação calculada 
	static float OriCal[3];

    int i;
    
    //+-------------------------------------------------------------------------------+
    // Identificando as marcas
    // ^^^^^^^^^^^^^^^^^^^^^^^
	if( id == marker_1 ){

		double roll, pitch, yaw;
		tf::Quaternion q(msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w);
		tf::Matrix3x3 m(q);

		Pos1[0] = msg.pose.position.x;
		Pos1[1] = msg.pose.position.y;
		Pos1[2] = msg.pose.position.z;

		Ori1[0] = msg.pose.orientation.x;
		Ori1[1] = msg.pose.orientation.y;
		Ori1[2] = msg.pose.orientation.z;

 		//std::cout << "ID:" << id << std::endl;
        //std::cout << "Marker " << marker_1 << " identificado" << std::endl;
        
	}
	if( id == marker_2 ){

		double roll, pitch, yaw;
		tf::Quaternion q(msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w);
		tf::Matrix3x3 m(q);

		Pos2[0] = msg.pose.position.x;
		Pos2[1] = msg.pose.position.y;
		Pos2[2] = msg.pose.position.z;

		Ori2[0] = msg.pose.orientation.x;
		Ori2[1] = msg.pose.orientation.y;
		Ori2[2] = msg.pose.orientation.z;

		//std::cout << "ID:" << id << std::endl;
		//std::cout << "Marker " << marker_2 << " identificado" << std::endl;
        
	}
	if( id == marker_3 ){

		double roll, pitch, yaw;
		tf::Quaternion q(msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w);
		tf::Matrix3x3 m(q);
		
		Pos3[0] = msg.pose.position.x;
		Pos3[1] = msg.pose.position.y;
		Pos3[2] = msg.pose.position.z;

		Ori3[0] = msg.pose.orientation.x;
		Ori3[1] = msg.pose.orientation.y;
		Ori3[2] = msg.pose.orientation.z;

		//std::cout << "ID:" << id << std::endl;
		//std::cout << "Marker " << marker_3 << " identificado" << std::endl;
        
	}
    //+-------------------------------------------------------------------------------+
    // Cálculo da orientação
    // ^^^^^^^^^^^^^^^^^^^^^
	// Vetores do marker 2 ao 1 e do marker 2 ao 3
	for( i=0 ; i<3 ; i++ ){
		vet_12[i] = Pos1[i] - Pos2[i];
		vet_32[i] = Pos3[i] - Pos2[i];
	}
	// Módulo do vetor vet_12
	mod_12 = mod_vet(vet_12);
	// Produto escalar -> vet_32  . vet_12
	for( i=0 ; i<3 ; i++ ){
		prod_es += vet_32[i]*vet_12[i];
	}
	// Projeção de vet_32 em vet_12
	for( i=0 ; i<3 ; i++ ){
		vet_proj[i] = ( prod_es / pow(mod_12, 2) ) * vet_12[i];
	}
	// Módulo do vetor vet_proj
	mod_proj = mod_vet(vet_proj);
	// Ortogonalização de Gram-Schimidf
	for( i =0 ; i<3 ; i++){
		vet_gram[i] =  vet_32[i] - vet_proj[i];
	}
	// Módulo do vetor vet_gram
	mod_gram = mod_vet(vet_gram);
	// Vetores normalizadoss
	for( i=0 ; i<3 ; i++ ){
		norm_proj[i] = vet_proj[i]/mod_proj;
		norm_gram[1] = vet_gram[i]/mod_gram;
	}
	// Produto vetorial ->  norm_gram X norm_proj
	prod_vet[0] = ( norm_gram[1]*norm_proj[2] ) - ( norm_gram[2]*norm_proj[1] );
	prod_vet[1] = ( norm_gram[2]*norm_proj[0] ) - ( norm_gram[0]*norm_proj[2] );
	prod_vet[2] = ( norm_gram[0]*norm_proj[1] ) - ( norm_gram[1]*norm_proj[0] );
	// Extrair a orientação:
	for( i=0 ; i<3 ; i++ ){
		OriCal[i]  = norm_proj[i]+ norm_gram[i]+ prod_vet[i]; 
	}		
	//+-------------------------------------------------------------------------------+
    // Prints
    // ^^^^^^

    // os vetores posições  
	//std::cout << "Position marker " << marker_1 << " : " << "[ " << Pos1[0] << " , " << Pos1[1] << " , " << Pos1[2] << " ]" << std::endl;
	//std::cout << "Position marker " << marker_2 << " : " << "[ " << Pos2[0] << " , " << Pos2[1] << " , " << Pos2[2] << " ]" << std::endl;
	//std::cout << "Position marker " << marker_3 << " : " << "[ " << Pos3[0] << " , " << Pos3[1] << " , " << Pos3[2] << " ]" << std::endl;

	// os vetores 
	//std::cout << "Vetor vet_12 : " << "[ " << vet_12[0] << " , " << vet_12[1] << " , " << vet_12[2] << " ]" << std::endl;
	//std::cout << "Vetor vet_32 : " << "[ " << vet_32[0] << " , " << vet_32[1] << " , " << vet_32[2] << " ]" << std::endl;
	//std::cout << "Vetor vet_proj : " << "[ " << vet_proj[0] << " , " << vet_proj[1] << " , " << vet_proj[2] << " ]" << std::endl;
	//std::cout << "Vetor vet_gram : " << "[ " << vet_gram[0] << " , " << vet_gram[1] << " , " << vet_gram[2] << " ]" << std::endl;

	// os módulos
	//std::cout << "Módulo de vet_proj : " << mod_proj << std::endl;
	//std::cout << "Módulo de vet_gram : " << mod_gram << std::endl;

	// os vetores normalizados
	//std::cout << "Vetor vet_proj normalizado : " << "[ " << norm_proj[0] << " , " << norm_proj[1] << " , " << norm_proj[2] << " ]" << std::endl;
	//std::cout << "Vetor vet_gram normalizado : " << "[ " << norm_gram[0] << " , " << norm_gram[1] << " , " << norm_gram[2] << " ]" << std::endl;

	// os produtos
	//std::cout << "Produto escalar : vet_32  . vet_12 : " << prod_es << std::endl;
	//std::cout << "Produto vetorial : norm_gram X norm_proj : " << "[ " << prod_vet[0] << " , " << prod_vet[1] << " , " << prod_vet[2] << " ]" << std::endl;

	// os vetores orientações
	std::cout << "Orientation marker " << marker_1 << " : " << "[ " << Ori1[0] << " , " << Ori1[1] << " , " << Ori1[2] << " ]" << std::endl;
	std::cout << "Orientation marker " << marker_2 << " : " << "[ " << Ori2[0] << " , " << Ori2[1] << " , " << Ori2[2] << " ]" << std::endl;
	std::cout << "Orientation marker " << marker_3 << " : " << "[ " << Ori3[0] << " , " << Ori3[1] << " , " << Ori3[2] << " ]" << std::endl;
	std::cout << "Orientation calculada : " << "[ " << OriCal[0] << " , " << OriCal[1] << " , " << OriCal[2] << " ]" << std::endl;
	

}


int main(int argc, char **argv){
 
  ros::init(argc, argv, "posicao_aruco");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/Estimated_marker", 1, chatterCallback);

  ros::spin();

  return 0;

}
